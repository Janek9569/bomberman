#pragma once
#include "SFML\Graphics.hpp"
#include <string>
#include "enums.h"
class Item;
class Bomb;
class Item_type: public virtual sf::Drawable
{
public:
	Item_type();
	~Item_type();
	sf::Uint32 getId() const { return id; }
	void _setId(sf::Uint32 idd) { id = idd; }
	void _setTexture(sf::Texture* text) { spr.setTexture(*text); }
	bool& _getProp(Prop prp) { return prop[prp]; }

	bool getProp(Prop prp) const { return prop[prp]; }
	sf::Sprite getSprite() const { return spr; }

	virtual sf::Uint16 update() { return 0; }
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual Item_type* getItem_type() { return this; }
	virtual const Item_type* getItem_type() const { return this; }
	virtual Item* getItem() { return NULL; }
	virtual const Item* getItem() const { return NULL; }
	virtual Bomb* getBomb() { return NULL; }
	virtual const Bomb* getBomb() const { return NULL; }

protected:
	sf::Uint32 id;
	bool prop[PROP_LAST];
	sf::Sprite spr;
};

