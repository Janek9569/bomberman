#include "Environment.h"
#include "scene.h"
#include <fstream>
#include <iostream>
#include "Random.h"
#include "Bomb.h"
scene::scene():
	window(sf::VideoMode(X*32, Y*32), "Bomberman!")
{
	gamer = nullptr;
	barrels = 0;
}


scene::~scene()
{
	for (int i = 0;i < X;++i)
		for (int j = 0; j < Y;++j)
			for (std::list<Item*>::iterator it = itemsOnScene[j][i].begin(); it != itemsOnScene[j][i].end(); ++it)
				delete (*it);
	for (std::list<Player*>::iterator it = playersOnScene.begin(); it != playersOnScene.end(); ++it)
		delete (*it);
	for (std::list<Animation*>::iterator it = animationsOnScene.begin(); it != animationsOnScene.end(); ++it)
		delete (*it);
}

bool isEmpty(const Item* itm) { return !itm; }

void scene::print()
{
	for (int i = 0;i < X;++i)
		for (int j = 0; j < Y;++j)
		{
			for (std::list<Item*>::iterator it = itemsOnScene[j][i].begin(); it != itemsOnScene[j][i].end(); ++it)
			{
				Item* item = (*it);
				if (*it)
				{
					Bomb* bomb = item->getBomb();
					if (bomb)
					{
						bomb->update();
						if (bomb->isAnimatiionEnded())
						{
							eventHandler.onItemDestroy(bomb, bomb->Item::getMapPos());
							delete bomb;
							(*it) = nullptr;
						}
					}
					if (*it)
					{
						item->setPos((float)i*FRAME_Y, (float)j*FRAME_X);
						window.draw(*item);
					}
				}
			}
			itemsOnScene[j][i].remove_if(isEmpty);
		}
	for (std::list<Player*>::iterator it = playersOnScene.begin(); it != playersOnScene.end(); ++it)
	{
		if (*it)
			window.draw(**it);
	}
	for (std::list<Animation*>::iterator it = animationsOnScene.begin(); it != animationsOnScene.end(); ++it)
	{
		if(*it)
			if (!(*it)->isAnimatiionEnded())
				window.draw(**it);
	}
}

void scene::addItem(Item * itm,sf::Uint16 x, sf::Uint16 y)
{
	itemsOnScene[y][x].push_back(itm);
}

void scene::addItem(Item * itm, sf::Vector2<sf::Uint16> pos)
{
	addItem(itm, pos.x, pos.y);
}

void scene::delItem(sf::Uint16 x, sf::Uint16 y)
{
	if (itemsOnScene[y][x].size())
	{
		Item* itm = itemsOnScene[y][x].back();
		itemsOnScene[y][x].pop_back();
		eventHandler.onItemDestroy(itm, itm->getMapPos());
		delete itm;
	}
}

void scene::delItemWithUid(sf::Uint16 x, sf::Uint16 y, sf::Uint64 uid)
{
	for (std::list<Item*>::iterator it = itemsOnScene[y][x].begin(); it != itemsOnScene[y][x].end(); ++it)
	{
		Item* itm = *it;
		if (itm->uid == uid)
		{
			eventHandler.onItemDestroy(itm, itm->getMapPos());
			delete itm;
			itemsOnScene[y][x].erase(it);
			return;
		}
	}
}

void scene::delItemWithUid(sf::Vector2<sf::Uint16> pos, sf::Uint64 uid)
{
	delItemWithUid(pos.x, pos.y, uid);
}

void scene::delItemWithId(sf::Vector2<sf::Uint16> pos, sf::Uint32 id)
{
	for (std::list<Item*>::iterator it = itemsOnScene[pos.y][pos.x].begin(); it != itemsOnScene[pos.y][pos.x].end(); ++it)
	{
		Item* itm = *it;
		if (itm->getId() == id)
		{
			eventHandler.onItemDestroy(itm, pos);
			delete itm;
			itemsOnScene[pos.y][pos.x].erase(it);
			return;
		}
	}
}

void scene::delItemWithId(sf::Uint16 x, sf::Uint16 y, sf::Uint32 id)
{
	delItemWithId(sf::Vector2<sf::Uint16>(x, y), id);
}

bool scene::isBlockingItem(sf::Vector2<sf::Uint16> pos)
{
	return isBlockingItem(pos.x, pos.y);
}

bool scene::isBlockingItem(sf::Uint16 x, sf::Uint16 y)
{
	bool isBlocking = false;
	for (std::list<Item*>::iterator it = itemsOnScene[y][x].begin(); it != itemsOnScene[y][x].end(); ++it)
	{
		Item* itm = *it;
		if(itm)
			if (!(itm->getProp(CAN_STEP_IN)))
			{
				isBlocking = true;
				break;
			}
	}
	return isBlocking;
}

bool scene::isBlockingStackItem(sf::Uint16 x, sf::Uint16 y)
{
	bool isBlocking = false;
	for (std::list<Item*>::iterator it = itemsOnScene[y][x].begin(); it != itemsOnScene[y][x].end(); ++it)
	{
		Item* itm = *it;
		if (itm->getProp(BLOCKING_STACK))
		{
			isBlocking = true;
			break;
		}
	}
	return isBlocking;
}

bool scene::isBlockingStackItem(const sf::Vector2<sf::Uint16>& pos)
{
	return isBlockingStackItem(pos.x, pos.y);
}

bool scene::willBeDangAnimOnSceneInNextTick(sf::Vector2<sf::Uint16> pos)
{
	std::list<Bomb*> bombs = getBombs();
	for (std::list<Bomb*>::iterator it = bombs.begin(); it != bombs.end();++it)
	{
		Bomb* b = *it;
		if (b->simExpl(sf::Vector2<sf::Uint16>(pos.x, pos.y)))
			return true;
	}
	return false;
}

Item* scene::isItemWithId(const sf::Vector2<sf::Uint16>& pos, sf::Uint32 id)
{
	for (std::list<Item*>::iterator it = itemsOnScene[pos.y][pos.x].begin(); it != itemsOnScene[pos.y][pos.x].end(); ++it)
	{
		Item* itm = *it;
		if (itm->getId() == id)
			return itm;
	}
	return nullptr;
}


bool scene::haveProp(sf::Uint16 x, sf::Uint16 y, Prop prop)
{
	bool haveProp = false;
	for (std::list<Item*>::iterator it = itemsOnScene[y][x].begin(); it != itemsOnScene[y][x].end(); ++it)
	{
		Item* itm = *it;
		if (itm->getProp(prop))
		{
			haveProp = true;
			break;
		}
	}
	return haveProp;
}

void scene::loadMap()
{
	std::ifstream file(MAP);
	int x, y;
	if (!(file >> x)) { std::cerr << "scene::loadMap() x problem\n"; return; }
	if (!(file >> y)) { std::cerr << "scene::loadMap() y problem\n"; return; }
	if (x != MAP_X || y != MAP_Y) { std::cerr << "scene::loadMap() wrong dimensions\n"; return; }
	for (uint16_t i = MAP_OFFSET; i < X-MAP_OFFSET;++i)
		for (uint16_t j = MAP_OFFSET;j < Y-MAP_OFFSET;++j)
		{
			uint16_t id;
			if(!(file >> id)) { std::cerr << "scene::loadMap() id loading problem\n"; return; }
			Item* newItem = new Item(id);
			addItem(newItem, i, j);
		}
	Random<sf::Uint16,0,100> random;
	for (uint16_t i = MAP_OFFSET; i < X - MAP_OFFSET;++i)
		for (uint16_t j = MAP_OFFSET;j < Y - MAP_OFFSET;++j)
		{
			if (!isBlockingStackItem(i,j))
			{
				bool canPlace = true;
				for (std::list<Player*>::iterator it = playersOnScene.begin(); it != playersOnScene.end(); ++it)
				{
					sf::Vector2<sf::Uint16> pPos = (*it)->getPosOnMap();
					if (((i <= pPos.x + 1) && (i >= pPos.x - 1)) && ((j >= pPos.y - 1) && (j <= pPos.y + 1)))
						canPlace = false;
				}
				if (canPlace)
				{
					if (random.rand() > 100-BARREL_CHANCE)
					{
						Item* newItem = new Item(BARREL);
						addItem(newItem, i, j);
						++barrels;
					}
				}
			}
		}
}

Player * scene::getPlayerOnScene(sf::Uint16 number)
{
	if (number < playersOnScene.size())
	{
		std::list<Player*>::iterator it = playersOnScene.begin();
		for (uint8_t i = 0;i < number; ++it);
		return *it;
	}
	return nullptr;
}

Player * scene::getPlayer(sf::Vector2<sf::Uint16> pos)
{
	for (std::list<Player*>::iterator it = playersOnScene.begin(); it != playersOnScene.end(); ++it)
	{
		if ((*it)->getPosOnMap() == pos)
			return *it;
	}
	return nullptr;
}

std::list<Bomb*> scene::getBombs()
{
	std::list<Bomb*> bombs;
	for (int i = 0;i < X;++i)
		for (int j = 0; j < Y;++j)
		{
			for (std::list<Item*>::iterator it = itemsOnScene[j][i].begin(); it != itemsOnScene[j][i].end(); ++it)
			{
				Item* item = (*it);
				if (*it)
				{
					Bomb* bomb = item->getBomb();
					if (bomb)
					{
						bombs.push_back(bomb);
					}
				}
			}
		}
	return bombs;
}

void scene::addPlayer(PlayerNumber playerNum, sf::Uint16 x, sf::Uint16 y)
{
	Player* player = new Player(playerNum);
	player->setPosOnMap(x, y);
	if (playersOnScene.size() == 0) gamer = player;
	playersOnScene.push_back(player);
	eventHandler.onSpawn(player, player->getPosOnMap());
}

bool isEnded(const Animation* anim) 
{ 
	if (anim->isAnimatiionEnded())
	{
		delete anim;
		return true;
	}
	return false;
}

bool haventHp(const Player* player) 
{ 
	if (player->getHp() <= 0)
	{
		eventHandler.onDie(player);
		delete player;
		return true;
	}
	return false;
}

void scene::update()
{
	if (env.isThinkin())
	{
		env.thinkTimer.restart();
		eventHandler.onThink();
	}
	animationsOnScene.remove_if(isEnded);
	for (std::list<Animation*>::iterator it = animationsOnScene.begin(); it != animationsOnScene.end(); ++it)
	{
		if (*it)
		{
			(*it)->update();
		}
	}
	for (std::list<Player*>::iterator it = playersOnScene.begin(); it != playersOnScene.end(); ++it)
	{
		if (*it)
		{
			(*it)->update();
		}
	}
	playersOnScene.remove_if(haventHp);
}

Prop findProp;
bool itHaveProp(const Item* item) 
{
	if (item->getProp(findProp) && (findProp == DESTROYABLE))
	{
		env.scen->substractBarrel();
		eventHandler.onItemDestroy((Item*)item, item->getMapPos());
		return true;
	}
	return false;
}

void scene::delItemsWithProp(sf::Uint16 x, sf::Uint16 y, Prop prop)
{
	findProp = prop;
	(itemsOnScene[y][x]).remove_if(itHaveProp);
}

bool scene::isDangAnimOnScene(sf::Vector2<sf::Uint16> pos)
{
#define DANG_ANIM_MIN 9
#define DANG_ANIM_MAX 15
	for (auto it = animationsOnScene.begin(); it != animationsOnScene.end(); ++it)
	{
		if ((*it)->getMapPos() == pos)
		{
			uint16_t id = (*it)->getAnimTextureId();
				if ((id >= DANG_ANIM_MIN) && (id <= DANG_ANIM_MAX))
					return true;
		}
	}
	return false;
}


