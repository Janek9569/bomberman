#include "Environment.h"
#include "Animation.h"

const sf::Vector2<sf::Uint16> operator *(sf::Vector2<sf::Uint16> vect, int num)
{
	return sf::Vector2<sf::Uint16>(vect.x * num, vect.y*num); 
}

const sf::Vector2<sf::Uint16> operator *(sf::Vector2<sf::Int16> vect, int num)
{
	return sf::Vector2<sf::Uint16>(vect.x * num, vect.y*num);
}

Animation::Animation()
{
	frame = 0;
	animTimer = sf::seconds(0);
	canRepeat = false;
	timeForAnimFrame= ANIM_STEP_TIME;
	ended = false;
	hurting = false;
	animTextureId = 0;
}


Animation::~Animation()
{
}

void Animation::setTexture(sf::Uint16 id)
{
	animTextureId = id;
	texture = env.getTexture(id);
	sprite.setTexture(*texture);
	frames = sprite.getTexture()->getSize().x / FRAME;
	sprite.setTextureRect(sf::IntRect(0, 0, ANIM_WIDH, ANIM_HIGH));
}

const sf::Vector2<sf::Uint16> Animation::getMapPos() const
{
	return sf::Vector2<sf::Uint16>((sf::Uint16)sprite.getPosition().x / FRAME, (sf::Uint16)sprite.getPosition().y / FRAME);
}

void Animation::setMapPos(sf::Vector2<sf::Uint16> mapPos)
{
	mapPos = mapPos*FRAME; setAnimPos((sf::Vector2f) mapPos);
}

void Animation::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	env.scen->window.draw(sprite);
}

sf::Uint16 Animation::update()
{
	if (ended) return frame;
	animTimer += sf::milliseconds(TICK);
	if (animTimer.asMilliseconds() > timeForAnimFrame)
	{
		animTimer = animTimer.Zero;
		++frame;
		if (frame == frames)
		{
			animTimer = animTimer.Zero;
			if (canRepeat) frame = 0;
			else
				ended = true;
		}
		sprite.setTextureRect(sf::IntRect(frame*FRAME, 0, ANIM_WIDH, ANIM_HIGH));
	}
	return frame;
}
