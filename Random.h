#pragma once
#include <random>
template <typename T, int a, int b>
class Random
{
public:
	Random() : e1(rd()), uniform_dist(a, b) {};
	~Random() {};

	T rand() { return uniform_dist(e1); }
private:
	std::random_device rd;
	std::default_random_engine e1;
	std::uniform_int_distribution<T> uniform_dist;
};


