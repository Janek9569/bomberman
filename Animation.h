#pragma once
#include "SFML\Graphics.hpp"

#define ANIM_WIDH 32
#define ANIM_HIGH 32

#define ANIM_STEP_TIME 500 // ms

const sf::Vector2<sf::Uint16> operator *(sf::Vector2<sf::Uint16> vect, int num);
const sf::Vector2<sf::Uint16> operator *(sf::Vector2<sf::Int16> vect, int num);
class Animation:
	public virtual sf::Drawable
{
public:
	Animation();
	~Animation();

	void setTexture(sf::Uint16 id);
	void setAnimPos(sf::Vector2f newPos) { sprite.setPosition(newPos); }
	void setIsHuting() { hurting = true; }

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual sf::Uint16 update();

	const sf::Vector2<sf::Uint16> getMapPos() const;
	void setMapPos(sf::Vector2<sf::Uint16> mapPos);
	const bool canRepeatAnimation() const { return canRepeat; }
	void allowToRepeatAnimation() { canRepeat = true; }
	const bool isAnimatiionEnded()const { return ended; }
	bool isHurting() const { return hurting; }
	const uint16_t getAnimTextureId() const { return animTextureId; }
	const sf::Uint32 getTimeForAnimFrame() const { return timeForAnimFrame; }
	void setTimeForAnimFrame(sf::Uint32 time) { timeForAnimFrame = time; }
protected:
	sf::Uint16 frames;
	sf::Uint16 frame;
	uint16_t animTextureId;
	sf::Int32 timeForAnimFrame;
	bool canRepeat;
	bool ended;
	bool hurting;
	sf::Time animTimer;

	sf::Texture* texture;
	sf::Sprite sprite;
};

