#pragma once
#include <boost/signals2.hpp>
#include "Player.h"
#include "Animation.h"
#include "enums.h"

typedef boost::signals2::signal<void(Player* who, sf::Vector2<sf::Uint16> toPos, Dir dir)> OnStepIn;
typedef OnStepIn::slot_type OnStepInSlotType;

typedef boost::signals2::signal<void(Item* itm, sf::Vector2<sf::Uint16> Pos)> OnItemDestroy;
typedef OnItemDestroy::slot_type OnItemDestroySlotType;

typedef boost::signals2::signal<void(Player* player, sf::Vector2<sf::Uint16> Pos)> OnSpawn;
typedef OnSpawn::slot_type OnSpawnSlotType;

typedef boost::signals2::signal<void(void)> OnThink;
typedef OnThink::slot_type OnThinkSlotType;

typedef boost::signals2::signal<void(const Player* player)> OnDie;
typedef OnDie::slot_type OnDieSlotType;

typedef boost::signals2::signal<void(Animation* anim)> OnAnimAdd;
typedef OnAnimAdd::slot_type OnAnimAddSlotType;

typedef boost::signals2::signal<void()> OnTick;
typedef OnTick::slot_type OnTickSlotType;

typedef boost::signals2::signal<void(sf::Keyboard::Key key)> OnButtonPressed;
typedef OnButtonPressed::slot_type OnButtonPressedSlotType;

typedef boost::signals2::signal<void(sf::Keyboard::Key key)> OnButtonReleased;
typedef OnButtonReleased::slot_type OnButtonReleasedSlotType;

class EventHandler
{
public:
	/**/
	static void stepIn(Player* who,sf::Vector2<sf::Uint16> toPos,Dir dir);
	static void itemDestroy(Item* itm, sf::Vector2<sf::Uint16> Pos);
	static void spawn(Player* player, sf::Vector2<sf::Uint16> toPos);
	static void think(void);
	static void die(const Player* player);
	static void animAdd(Animation* anim);
	static void tick();
	static void pressButton(sf::Keyboard::Key key);
	static void releaseButton(sf::Keyboard::Key key);
	/**/

	/**********/
	OnItemDestroy* getOnItemDestroy() { return &onItemDestroy; }
	OnItemDestroy onItemDestroy;

	OnStepIn* getOnStepIn() { return &onStepIn; }
	OnStepIn onStepIn;

	OnSpawn* getOnSpawn() { return &onSpawn; }
	OnSpawn onSpawn;

	OnThink* getOnThink() { return &onThink; }
	OnThink onThink;

	OnDie* getOnDie() { return &onDie; }
	OnDie onDie;

	OnAnimAdd* getOnAnimAdd() { return &onAnimAdd; }
	OnAnimAdd onAnimAdd;

	OnTick* getOnTick() { return &onTick; }
	OnTick onTick;

	OnButtonPressed* getOnButtonPressed() { return &onButtonPressed; }
	OnButtonPressed onButtonPressed;

	OnButtonReleased* getOnButtonReleased() { return &onButtonReleased; }
	OnButtonReleased onButtonReleased;
	/**********/

	EventHandler();
	~EventHandler();
};

extern EventHandler eventHandler;
