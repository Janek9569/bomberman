#pragma once
#include "Item.h"
#include <SFML/Graphics.hpp>
#include <vector>
#include <list>
#include "Player.h"
#include "Animation.h"

#define FRAME_X 32
#define FRAME_Y 32
#define X 17
#define Y 17
#define MAP_OFFSET 2
#define MAP_X (X-2*MAP_OFFSET)
#define MAP_Y (Y-2*MAP_OFFSET)

#define ARENA_TOP_X 2*FRAME_X
#define ARENA_TOP_Y 2*FRAME_Y
#define ARENA_BOTTOM_X (13 + 1)*FRAME_X
#define ARENA_BOTTOM_Y (13 + 1)*FRAME_Y

#define MAP "Map/map.prp"
#define BARREL_CHANCE 75
#define BACKGROUND 2
#define BARREL 1
#define BLOCKING_BLOCK 0

class scene
{
public:
	scene();
	~scene();

	void print();
	void addItem(Item * itm, sf::Uint16 x, sf::Uint16 y);
	void addItem(Item * itm, sf::Vector2<sf::Uint16> pos);
	void addPlayer(PlayerNumber playerNum, sf::Uint16 x, sf::Uint16 y);
	void addAnimation(Animation* anim) { if (anim) animationsOnScene.push_back(anim); }

	void delItem(sf::Uint16 x, sf::Uint16 y);
	void delItemWithUid(sf::Uint16 x, sf::Uint16 y, sf::Uint64 uid);
	void delItemWithUid(sf::Vector2<sf::Uint16> pos, sf::Uint64 uid);
	void delItemWithId(sf::Vector2<sf::Uint16> pos, sf::Uint32 id);
	void delItemWithId(sf::Uint16 x, sf::Uint16 y, sf::Uint32 id);
	void delItemsWithProp(sf::Uint16 x, sf::Uint16 y, Prop prop);

	bool isBlockingItem(sf::Vector2<sf::Uint16> pos);
	bool isBlockingItem(sf::Uint16 x, sf::Uint16 y);
	bool isBlockingItem(uint32_t x, uint32_t y) { return isBlockingItem((sf::Uint16)x, (sf::Uint16)y); }
	bool isBlockingItem(sf::Vector2<int32_t> pos) { return isBlockingItem(sf::Vector2<sf::Uint16>((sf::Uint16)pos.x, (sf::Uint16)pos.y)); }
	bool isBlockingStackItem(sf::Uint16 x, sf::Uint16 y);
	bool isBlockingStackItem(const sf::Vector2<sf::Uint16>& pos);
	bool isDangAnimOnScene(sf::Vector2<sf::Uint16> pos);
	bool _isEmpty(sf::Vector2<sf::Uint16> pos) { if (itemsOnScene[pos.y][pos.x].size()) return false;return true; }
	bool willBeDangAnimOnSceneInNextTick(sf::Vector2<sf::Uint16> pos);
	Item* isItemWithId(const sf::Vector2<sf::Uint16>& pos, sf::Uint32 id);
	bool haveProp(sf::Uint16 x, sf::Uint16 y, Prop prop);

	sf::RenderWindow window;
	void update();
	void loadMap();

	Player* getPlayerOnScene(sf::Uint16 number);
	Player* getPlayer(sf::Vector2<sf::Uint16> pos);
	Player* getGamer() { return gamer; }
	std::list<Bomb*> getBombs();
	void setGamer(Player* player) { gamer = player; }
	void substractBarrel() { if (barrels) --barrels; }

	sf::Uint32 getBarrels() { return barrels; }
	size_t getPlayersOnScene()const { return playersOnScene.size(); }

	std::list<Player*>::iterator pBegin() { return playersOnScene.begin(); }
	std::list<Player*>::iterator pEnd() { return playersOnScene.end(); }

private:
	sf::Uint32 barrels;
	std::list<Item*> itemsOnScene[Y][X];
	std::list<Player*> playersOnScene;
	std::list<Animation*> animationsOnScene;
	Player* gamer;
};

