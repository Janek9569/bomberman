#include "Environment.h"
#include "scene.h"
#include "Item.h"
#include "Random.h"
#include <iostream>

sf::Uint64 _uidIter = 0;

Item::Item(sf::Uint16 idd)
{
	if (_uidIter == 0)
		eventHandler.onItemDestroy.connect(itemDestroy);
	uid = _uidIter++;
	id = idd;
	Item_type* typ = env.getItemType(idd);
	for (int i = 0; i < PROP_LAST;++i)
		prop[i] = typ->getProp((Prop)i);
	spr = typ->getSprite();
}

Item::Item()
{
}

Item::~Item()
{
}

sf::Vector2<sf::Uint16> Item::getMapPos() const
{
	return sf::Vector2<sf::Uint16>((sf::Uint16)spr.getPosition().x / FRAME, (sf::Uint16)spr.getPosition().y / FRAME);
}

void Item::itemDestroy(Item * itm, sf::Vector2<sf::Uint16> Pos)
{
	if (itm->getId() == BARREL)
	{
		Random<sf::Uint16, 0, 100> rand;
		if (rand.rand() <= BONUS_CHANCE)
		{
			Item* bonus = new Item((rand.rand() % 3 + 4));
			env.scen->addItem(bonus, Pos);
		}
	}
}