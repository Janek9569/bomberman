#pragma once
#include "Player.h"
#include "SFML\Main.hpp"
class OppEngine
{
public:
	OppEngine();
	~OppEngine();

	static void spawn(Player* player, sf::Vector2<sf::Uint16> toPos);
	static void stepIn(Player* who, sf::Vector2<sf::Uint16> toPos, Dir dir);
};

