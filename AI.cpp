#include "Environment.h"
#include "AI.h"
#include "Bomb.h"
#include <map>
#include <memory>

AI::AI()
{
}


AI::~AI()
{
}

bool operator==(const std::shared_ptr<A_field> x, const std::shared_ptr<A_field> y)
{
	if ((x->x == y->x) && (x->y == y->y))
		return true;
	return false;
}

bool operator==(const _MapPoint x, const _MapPoint y)
{
	if ((x.x == y.x) && (x.y == y.y))
		return true;
	return false;
}

uint16_t A_field::_howmManyFields()
{
	uint16_t num = 1;
	auto par = parent;
	while (par)
	{
		++num;
		par = par->parent;
	}
	return num;
}

bool A_field::A_isMined() const
{
	std::list<Bomb*> bombs = env.scen->getBombs();
	for (std::list<Bomb*>::iterator it = bombs.begin(); it != bombs.end();++it)
	{
		Bomb* b = *it;
		if (!b->isSafe())
			if (b->simExpl(sf::Vector2<sf::Uint16>(x, y)))
				return true;
	}
	return false;
}

bool A_field::A_canStep() const
{
	return !env.scen->isBlockingItem(x, y);
}

bool A_field::A_isBrick() const
{
	return (env.scen->isItemWithId(vec(), BLOCKING_BLOCK));
}

bool A_field::A_inDang(Bomb* bb)
{
	if(bb)
		if (bb->simExpl(sf::Vector2<sf::Uint16>(x, y)))
			return true;
	if (env.scen->isDangAnimOnScene(sf::Vector2<sf::Uint16>(x, y))) return true;
	if (env.scen->willBeDangAnimOnSceneInNextTick(sf::Vector2<sf::Uint16>(x, y))) return true;
	std::list<Bomb*> bombs = env.scen->getBombs();
	for (std::list<Bomb*>::iterator it = bombs.begin(); it != bombs.end();++it)
	{
		Bomb* b = *it;
		if(!b->isSafe())
			if (b->simExpl(sf::Vector2<sf::Uint16>(x, y)))
				return true;
	}
	return false;
}

bool A_field::A_isEmpty() const
{
	if (env.scen->_isEmpty(vec())) return true;
	return false;
}

bool A_field::A_isPlayer() const
{
	if (env.scen->getPlayer(vec())) return true;
	return false;
}

bool AI::_isOnList(std::shared_ptr<A_field> field, std::list<std::shared_ptr<A_field>>& list)
{
	for (std::list<std::shared_ptr<A_field>>::iterator it = list.begin();it != list.end();++it)
		if (*it == field) return true;
	return false;
}

struct equalField 
{
	equalField(std::shared_ptr<A_field> x) :_x(x) {  }
	std::shared_ptr<A_field> _x;
	bool operator() (std::shared_ptr<A_field>& field)
	{ 
		if (field == _x)
		{
			return true;
		}
		return false;
	}
};

void AI::_moveToList(std::shared_ptr<A_field> x, std::list<std::shared_ptr<A_field>>& from, std::list<std::shared_ptr<A_field>>& to)
{
	from.remove_if(equalField(x));
	to.push_back(std::shared_ptr<A_field>(x));
}


std::list<_MapPoint> AI::_makeTrace(std::shared_ptr<A_field> dest)
{
	std::list<_MapPoint>trace;
	A_field* it = dest._Get();
	while (it != nullptr)
	{
		trace.push_front(_MapPoint(it));
		it = it->parent._Get();
	}
	if(trace.size())
		trace.pop_front();
	return trace;
}

std::list<std::shared_ptr<A_field>> AI::_getNeighbors(std::shared_ptr<A_field> field)
{
	std::list<std::shared_ptr<A_field>> neighbors;
	if (field->x > MAP_OFFSET) neighbors.push_back(std::shared_ptr<A_field>(new A_field(field->x - 1, field->y, field->dest)));
	if (field->y > MAP_OFFSET) neighbors.push_back(std::shared_ptr<A_field>(new A_field(field->x, field->y-1, field->dest)));
	if (field->x < X-MAP_OFFSET)neighbors.push_back(std::shared_ptr<A_field>(new A_field(field->x + 1, field->y, field->dest)));
	if (field->y < Y-MAP_OFFSET)neighbors.push_back(std::shared_ptr<A_field>(new A_field(field->x, field->y + 1, field->dest)));
	return neighbors;
}

std::list<_MapPoint> AI::A_getTrace(sf::Vector2<sf::Uint16> frm, sf::Vector2<sf::Uint16> to)
{
	std::shared_ptr<A_field> from(new A_field(frm.x, frm.y, to));
	std::shared_ptr<A_field> dest(new A_field(to.x, to.y, to));

	std::list<std::shared_ptr<A_field>> A_openList;
	std::list<std::shared_ptr<A_field>> A_closedList;
	std::list<std::shared_ptr<A_field>> A_findFields;
	A_findFields.push_back(std::shared_ptr<A_field>(new A_field(to.x - 1, to.y, to)));
	A_findFields.push_back(std::shared_ptr<A_field>(new A_field(to.x + 1, to.y, to)));
	A_findFields.push_back(std::shared_ptr<A_field>(new A_field(to.x, to.y - 1, to)));
	A_findFields.push_back(std::shared_ptr<A_field>(new A_field(to.x, to.y + 1, to)));

	A_openList.push_back(std::shared_ptr<A_field>(from));
	while (A_openList.size())
	{
		std::shared_ptr<A_field> x = _getLowestTotalScore(A_openList);
		_moveToList(x, A_openList, A_closedList);
		if (x == dest || _isOnList(x, A_findFields))//znaleziono najkrutsza sciezke
		{
			std::list<_MapPoint> trace = _makeTrace(x);
			return trace;
		}
		std::list<std::shared_ptr<A_field>> neigh = _getNeighbors(x);
		for (std::list<std::shared_ptr<A_field>>::iterator it = neigh.begin();it != neigh.end();++it)//sasiedzi x
		{	//jest na zamknietej licie lub nie mozna wejsc)
			if ((!(*it)->A_canStep()) || (_isOnList(*it,A_closedList)))
			{
				continue;
			}
			else if(!_isOnList(*it,A_openList))//nie znajduje sie na otwartej liscie
			{
				A_openList.push_back(*it);//przenies do otwartych
				(*it)->parent = x;//x rodzicem tego sasiada
				(*it)->update();//aktualizuj sasiada
			}
			else
			{
				uint16_t newG = (*it)->_getCost();//oblicz nowa wartosc G sasiada
				if (newG < (*it)->cost)//nowaG<G
				{
					(*it)->parent = x;//x rodzicem sasiada
					(*it)->cost = newG;//G sasiada = nowaG
									//oblicz nowa wartosc Fsasiada
				}
			}
		}
	}
	return std::list<_MapPoint>(); // brak sciezki
}

std::list<_MapPoint> AI::A_getSafeTrace(sf::Vector2<sf::Uint16> frm, sf::Vector2<sf::Uint16> to,uint8_t dontCare)
{
	std::shared_ptr<A_field> from(new A_field(frm.x, frm.y, to));
	std::shared_ptr<A_field> dest(new A_field(to.x, to.y, to));

	std::list<std::shared_ptr<A_field>> A_openList;
	std::list<std::shared_ptr<A_field>> A_closedList;

	A_openList.push_back(std::shared_ptr<A_field>(from));
	while (A_openList.size())
	{
		std::shared_ptr<A_field> x = _getLowestTotalScore(A_openList);
		_moveToList(x, A_openList, A_closedList);
		if (x == dest && !x->A_inDang())//znaleziono najkrutsza sciezke
		{
			std::list<_MapPoint> trace = _makeTrace(x);
			return trace;
		}
		std::list<std::shared_ptr<A_field>> neigh = _getNeighbors(x);
		for (std::list<std::shared_ptr<A_field>>::iterator it = neigh.begin();it != neigh.end();++it)//sasiedzi x
		{	//jest na zamknietej licie lub nie mozna wejsc)
			if ((!(*it)->A_canStep()) || (_isOnList(*it, A_closedList)) || (*it)->A_inDang() || (*it)->A_isEmpty())
			{
				continue;
			}
			else if (!_isOnList(*it, A_openList))//nie znajduje sie na otwartej liscie
			{
				A_openList.push_back(*it);//przenies do otwartych
				(*it)->parent = x;//x rodzicem tego sasiada
				(*it)->update();//aktualizuj sasiada
			}
			else
			{
				uint16_t newG = (*it)->_getCost();//oblicz nowa wartosc G sasiada
				if (newG < (*it)->cost)//nowaG<G
				{
					(*it)->parent = x;//x rodzicem sasiada
					(*it)->cost = newG;//G sasiada = nowaG
									   //oblicz nowa wartosc Fsasiada
				}
			}
		}
	}
	return std::list<_MapPoint>(); // brak sciezki
}

std::list<_MapPoint> AI::A_getEscape(sf::Vector2<sf::Uint16> frm)
{
	std::shared_ptr<A_field> from(new A_field(frm.x, frm.y, frm));

	std::list<std::shared_ptr<A_field>> A_openList;
	std::list<std::shared_ptr<A_field>> A_closedList;

	A_openList.push_back(std::shared_ptr<A_field>(from));
	while (A_openList.size())
	{
		std::shared_ptr<A_field> x = _getLowestEscapeScore(A_openList);
		_moveToList(x, A_openList, A_closedList);
		if ((!x->A_inDang()) && (x->A_canStep()))//znaleziono najkrutsza sciezke
		{
			std::list<_MapPoint> trace = _makeTrace(x);
			return trace;
		}
		std::list<std::shared_ptr<A_field>> neigh = _getNeighbors(x);
		for (std::list<std::shared_ptr<A_field>>::iterator it = neigh.begin();it != neigh.end();++it)//sasiedzi x
		{	//jest na zamknietej licie lub nie mozna wejsc)
			if ((!(*it)->A_canStep()) || (_isOnList(*it, A_closedList)) || ((*it)->A_isEmpty()) || (*it)->A_isPlayer())
			{
				continue;
			}
			else if (!_isOnList(*it, A_openList))//nie znajduje sie na otwartej liscie
			{
				A_openList.push_back(*it);//przenies do otwartych
				(*it)->parent = x;//x rodzicem tego sasiada
				(*it)->update();//aktualizuj sasiada
			}
			else
			{
				uint16_t newG = (*it)->_getCost();//oblicz nowa wartosc G sasiada
				if (newG < (*it)->cost)//nowaG<G
				{
					(*it)->parent = x;//x rodzicem sasiada
					(*it)->cost = newG;//G sasiada = nowaG
									   //oblicz nowa wartosc Fsasiada
				}
			}
		}
	}
	return std::list<_MapPoint>(); // brak sciezki
}

std::list<_MapPoint> AI::A_getFutherEscape(sf::Vector2<sf::Uint16> frm, Bomb * b)
{
	std::shared_ptr<A_field> from(new A_field(frm.x, frm.y, frm));

	std::list<std::shared_ptr<A_field>> A_openList;
	std::list<std::shared_ptr<A_field>> A_closedList;
	 
	A_openList.push_back(std::shared_ptr<A_field>(from));
	while (A_openList.size())
	{
		std::shared_ptr<A_field> x = _getLowestEscapeScore(A_openList);
		_moveToList(x, A_openList, A_closedList);
		if ((!x->A_inDang(b)) && (x->A_canStep()))//znaleziono najkrutsza sciezke
		{
			std::list<_MapPoint> trace = _makeTrace(x);
			return trace;
		}
		std::list<std::shared_ptr<A_field>> neigh = _getNeighbors(x);
		for (std::list<std::shared_ptr<A_field>>::iterator it = neigh.begin();it != neigh.end();++it)//sasiedzi x
		{	//jest na zamknietej licie lub nie mozna wejsc)
			if ((!(*it)->A_canStep()) || (_isOnList(*it, A_closedList)) || ((*it)->A_isEmpty()) || (*it)->A_isPlayer() || (*it)->A_inDang())
			{
				continue;
			}
			else if (!_isOnList(*it, A_openList))//nie znajduje sie na otwartej liscie
			{
				A_openList.push_back(*it);//przenies do otwartych
				(*it)->parent = x;//x rodzicem tego sasiada
				(*it)->update();//aktualizuj sasiada
			}
			else
			{
				uint16_t newG = (*it)->_getCost();//oblicz nowa wartosc G sasiada
				if (newG < (*it)->cost)//nowaG<G
				{
					(*it)->parent = x;//x rodzicem sasiada
					(*it)->cost = newG;//G sasiada = nowaG
									   //oblicz nowa wartosc Fsasiada
				}
			}
		}
	}
	return std::list<_MapPoint>(); // brak sciezki
}

FollowType AI::_isThig(std::shared_ptr<A_field> p)
{
	scene* sc = env.scen;
	if (sc->isItemWithId(p->vec(), BARREL)) return A_BARREL;
	if (sc->haveProp(p->x, p->y, IS_BONUS)) return A_BONUS;
	Player* pl = sc->getPlayer(p->vec());
	if (pl)
	{
		if (pl->getPosOnMap() != p->dest) return A_PLAYER;
	}
	return A_NONE;
}

std::pair<std::list<_MapPoint>, FollowType> AI::A_getNearest(sf::Vector2<sf::Uint16> frm)
{

	std::shared_ptr<A_field> from(new A_field(frm.x, frm.y, frm));
	std::list<std::shared_ptr<A_field>> A_openList;
	std::list<std::shared_ptr<A_field>> A_closedList;

	A_openList.push_back(std::shared_ptr<A_field>(from));
	while (A_openList.size())
	{
		std::shared_ptr<A_field> x = _getLowestTotalScore(A_openList);
		_moveToList(x, A_openList, A_closedList);
		if (FollowType typ =_isThig(x))//znaleziono najkrutsza sciezke
		{
			if (typ == A_BARREL)
			{
				if (!x->A_isMined())
				{
					std::list<_MapPoint> trace = _makeTrace(x);
					return std::pair<std::list<_MapPoint>, FollowType>(trace, typ);
				}
			}
			else
			{
				std::list<_MapPoint> trace = _makeTrace(x);
				return std::pair<std::list<_MapPoint>, FollowType>(trace, typ);
			}
		}
		std::list<std::shared_ptr<A_field>> neigh = _getNeighbors(x);
		for (std::list<std::shared_ptr<A_field>>::iterator it = neigh.begin();it != neigh.end();++it)//sasiedzi x
		{	//jest na zamknietej licie lub nie mozna wejsc)
			if (((*it)->A_isBrick()) || (_isOnList(*it, A_closedList)) || ((*it)->A_isEmpty()))
			{
				continue;
			}
			else if (!_isOnList(*it, A_openList))//nie znajduje sie na otwartej liscie
			{
				A_openList.push_back(*it);//przenies do otwartych
				(*it)->parent = x;//x rodzicem tego sasiada
				(*it)->update();//aktualizuj sasiada
			}
			else
			{
				uint16_t newG = (*it)->_getCost();//oblicz nowa wartosc G sasiada
				if (newG < (*it)->cost)//nowaG<G
				{
					(*it)->parent = x;//x rodzicem sasiada
					(*it)->cost = newG;//G sasiada = nowaG
									   //oblicz nowa wartosc Fsasiada
				}
			}
		}
	}
	return std::pair<std::list<_MapPoint>, FollowType>(std::list<_MapPoint>(),A_NONE);
}

std::pair<std::list<_MapPoint>, FollowType> AI::A_getSafeNearest(sf::Vector2<sf::Uint16> frm)
{

	std::shared_ptr<A_field> from(new A_field(frm.x, frm.y, frm));
	std::list<std::shared_ptr<A_field>> A_openList;
	std::list<std::shared_ptr<A_field>> A_closedList;

	A_openList.push_back(std::shared_ptr<A_field>(from));
	while (A_openList.size())
	{
		std::shared_ptr<A_field> x = _getLowestTotalScore(A_openList);
		_moveToList(x, A_openList, A_closedList);
		if (FollowType typ = _isThig(x))//znaleziono najkrutsza sciezke
		{
			if (typ == A_BARREL)
			{
				if (!x->A_isMined())
				{
					std::list<_MapPoint> trace = _makeTrace(x);
					return std::pair<std::list<_MapPoint>, FollowType>(trace, typ);
				}
			}
			else
			{
				std::list<_MapPoint> trace = _makeTrace(x);
				return std::pair<std::list<_MapPoint>, FollowType>(trace, typ);
			}
		}
		std::list<std::shared_ptr<A_field>> neigh = _getNeighbors(x);
		for (std::list<std::shared_ptr<A_field>>::iterator it = neigh.begin();it != neigh.end();++it)//sasiedzi x
		{	//jest na zamknietej licie lub nie mozna wejsc)
			if (((*it)->A_isBrick()) || (_isOnList(*it, A_closedList)) || ((*it)->A_isEmpty()) || (*it)->A_inDang())
			{
				continue;
			}
			else if (!_isOnList(*it, A_openList))//nie znajduje sie na otwartej liscie
			{
				A_openList.push_back(*it);//przenies do otwartych
				(*it)->parent = x;//x rodzicem tego sasiada
				(*it)->update();//aktualizuj sasiada
			}
			else
			{
				uint16_t newG = (*it)->_getCost();//oblicz nowa wartosc G sasiada
				if (newG < (*it)->cost)//nowaG<G
				{
					(*it)->parent = x;//x rodzicem sasiada
					(*it)->cost = newG;//G sasiada = nowaG
									   //oblicz nowa wartosc Fsasiada
				}
			}
		}
	}
	return std::pair<std::list<_MapPoint>, FollowType>(std::list<_MapPoint>(), A_NONE);
}

std::shared_ptr<A_field> AI::_getLowestTotalScore(std::list<std::shared_ptr<A_field>>& A_openList)
{
	uint16_t totalScore = A_openList.back()->getTotalCost();
	std::shared_ptr<A_field> actField = A_openList.back();
	for (std::list<std::shared_ptr<A_field>>::iterator it = A_openList.begin(); it != A_openList.end();++it)
	{
		if (totalScore >= (*it)->getTotalCost())
		{
			totalScore = (*it)->getTotalCost(); actField = *it;
		}
	}
	return actField;
}

std::shared_ptr<A_field> AI::_getLowestEscapeScore(std::list<std::shared_ptr<A_field>>& A_openList)
{
	size_t totalScore = A_getNearest(A_openList.back()->vec()).first.size();
	std::shared_ptr<A_field> actField = A_openList.back();
	for (std::list<std::shared_ptr<A_field>>::iterator it = A_openList.begin(); it != A_openList.end();++it)
	{
		size_t newCost = A_getNearest((*it)->vec()).first.size();
		if (totalScore >= newCost)
		{
			totalScore = newCost; actField = *it;
		}
	}
	return actField;
}
