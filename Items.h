#pragma once
#include "SFML\Main.hpp"
#include "Item_type.h"
#include <map>
class Items
{
public:
	Items();
	~Items();
	std::map<sf::Uint32, Item_type> items;
	void loadItemsType();

};

