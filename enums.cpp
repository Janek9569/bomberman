#include "enums.h"
#include <map>
std::map<std::string, Prop> str2prop;

Orientation getOrientation(Dir dir)
{
	switch (dir)
	{
	case LEFT:
	case RIGHT:
		return HORIZONTAL;
	case UP:
	case DOWN:
		return VERTICAL;
	}
}

Dir versorToDir(volatile sf::Vector2<sf::Int16> versor)
{
	if (versor.x == -1 && versor.y == 0) return LEFT;
	if (versor.x == 1  && versor.y == 0) return RIGHT;
	if (versor.x == 0  && versor.y == 1) return DOWN;
	if (versor.x == 0 && versor.y == -1) return UP;
	return LEFT;
}