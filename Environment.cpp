#include "Environment.h"
#include <boost\filesystem.hpp>
#include "Items.h"
Environment env;
sf::Texture* invisible;
Environment::Environment()
{
	loadTextures();
	items.loadItemsType();
	scen = new scene;
	invisible = &env.Textures[16];
}


Environment::~Environment()
{
	if (scen)
		delete scen;
}

void Environment::loadTextures()
{
	/*
	volatile std::string name = it->path().stem().string(); // xxx
	volatile std::string name2 = it->path().filename().string(); //xxx.txt
	*/
	std::string fDir = "Textures/";
	boost::filesystem::path p(fDir);
	boost::filesystem::directory_iterator end_iter;
	if (exists(p) && is_directory(p))//folder istnieje
		for (boost::filesystem::directory_iterator it = boost::filesystem::directory_iterator(p); it != end_iter;it++)
			if (boost::filesystem::is_regular_file(it->status()))
			{
				std::string name = it->path().filename().string();
				sf::Texture newTexture;
				std::string exten = it->path().extension().string();
				if (exten == ".png")
				{
					newTexture.loadFromFile(fDir + name);
					Textures[atoi(name.c_str())] = newTexture;
				}
			}
}
