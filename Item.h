#pragma once
#include "Item_type.h"
#include "SFML\Graphics.hpp"

#define BONUS_CHANCE 30

class Item :
	public Item_type
{
public:
	Item(sf::Uint16 idd);
	Item();
	~Item();
	void setPos(sf::Vector2f pos) { spr.setPosition(pos); }
	void setPos(float x, float y) { spr.setPosition(x,y); }
	sf::Vector2<sf::Uint16> getMapPos() const;
	void setIMapPos(sf::Vector2<sf::Uint16> pos) { setPos(float(pos.x)*FRAME, float(pos.y)*FRAME); }
	virtual sf::Uint16 update() { return 0; }
	virtual Item* getItem() { return this; }
	virtual const Item* getItem() const { return this; }
	sf::Uint64 uid;
	static void itemDestroy(Item* itm, sf::Vector2<sf::Uint16> Pos);
};

