#include "Environment.h"
#include "Bomb.h"
#include <iostream>

Bomb::Bomb() : Item(BOMB_ID)
{
	setTexture(9);
	canRepeat = false;
	frame = 0;
	range = 5;
	exploded = false;
	isHurting = false;
}

Bomb::~Bomb()
{
}

sf::Uint16 Bomb::update()
{
	if (ended) return frame;
	animTimer += sf::milliseconds(TICK);
	if (animTimer.asMilliseconds() > timeForAnimFrame)
	{
		animTimer = animTimer.Zero;
		++frame;
		if (frame == EXPLODING_FRAME)
			explode();
		else if (frame == frames)
		{
			animTimer = animTimer.Zero;
			if (canRepeat) frame = 0;
			else
				ended = true;
		}
		else if (frames >= SMOKE_FRAMES)
			isHurting = false;
		sprite.setTextureRect(sf::IntRect(frame*FRAME, 0, ANIM_WIDH, ANIM_HIGH));
	}
	return frame;
}

sf::Vector2<sf::Int16> Bomb::getVersor(Dir dir)
{
	switch (dir)
	{
	case LEFT:		return sf::Vector2<sf::Int16>(-1, 0);		break;
	case RIGHT:		return sf::Vector2<sf::Int16>(1, 0);		break;
	case UP:		return sf::Vector2<sf::Int16>(0, -1);		break;
	case DOWN:		return sf::Vector2<sf::Int16>(0, 1);		break;
	}
	return sf::Vector2<sf::Int16>(0, 0);
}

bool Bomb::animOutOfScreen(const sf::Vector2<sf::Uint16> pos)
{
	if ((pos.x < MAP_OFFSET) || (pos.y < MAP_OFFSET) || (pos.x >= X - MAP_OFFSET) || (pos.y >= Y - MAP_OFFSET))
		return true;
	else return false;
}

bool Bomb::simExpl(sf::Vector2<sf::Uint16> pPos)
{
	sf::Vector2<sf::Uint16> pos = Item::getMapPos();
	if (pPos ==Item::getMapPos())
		return true;

	bool actDir[DIR_LAST] = { true,true,true,true };
	for (uint8_t i = 0; i < DIR_LAST;++i)
	{
		uint16_t actRange = 1;
		while (actDir[i] && (actRange <= range))
		{
			Dir iDir = (Dir)i;
			sf::Vector2<sf::Uint16> animPos = pos + getVersor(iDir)*actRange;
			if (!animOutOfScreen(animPos))
			{
				if (env.scen->isBlockingStackItem(animPos))
				{
					actDir[i] = false;
				}
				if (animPos == pPos) 
					return true;
			}
			else
			{
				actDir[i] = false;
			}
			++actRange;
		}
	}
	return false;
}

void Bomb::explode()
{
#define EXPL_TIME 200
	timeForAnimFrame = EXPL_TIME;
	if (Player* player = env.scen->getPlayer(Item::getMapPos()))
	{
		--(player->getHp());
		player->dmgInterval.restart();
	}
	exploded = true;
	frame = EXPLODING_FRAME;
	isHurting = true;
	prop[CAN_STEP_IN] = true;
	owner->addBombs(1);
	static const uint16_t textures[4][2] = { {VERTICAL_TEXTURE, DOWN_TEXTURE},{HORIZONTAL_TEXTURE, LEFT_TEXTURE},
											 {HORIZONTAL_TEXTURE, RIGHT_TEXTURE},{ VERTICAL_TEXTURE, UP_TEXTURE} };
	sf::Vector2<sf::Uint16> pos = Item::getMapPos();
	bool actDir[DIR_LAST] = { true,true,true,true };
	Animation* before = nullptr;
	for (uint8_t i = 0; i < DIR_LAST;++i)
	{
		uint16_t actRange = 1;
		while (actDir[i] && (actRange <= range))
		{
			Dir iDir = (Dir)i;
			sf::Vector2<sf::Uint16> animPos = pos + getVersor(iDir)*actRange;
			if (!animOutOfScreen(animPos))
			{
				Animation* anim = new Animation;
				anim->setTimeForAnimFrame(EXPL_TIME);
				before = anim;
				anim->setAnimPos(sf::Vector2f((float)(animPos.x)*FRAME, (float)animPos.y*FRAME));
				/*Item* itm;
				if (itm = env.scen->isItemWithId(animPos,BOMB_ID))
				{
					Bomb* b = itm->getBomb();
					if(!b->isExploded())
						b->explode();
				}*/
				if (!env.scen->isBlockingStackItem(animPos))
				{
					if (actRange == range)
						anim->setTexture(textures[iDir][1]);
					else
						anim->setTexture(textures[iDir][0]);
				}
				else
				{
					actDir[i] = false;
					anim->setTexture(textures[iDir][1]);
					env.scen->delItemWithId(animPos,BARREL);
				}
				env.scen->addAnimation(anim);
				anim->setIsHuting();
				eventHandler.onAnimAdd(anim);
			}
			else
			{
				actDir[i] = false;
				if (actRange > 1)
					before->setTexture(textures[iDir][1]);
			}
			++actRange;
		}
	}
}
void Bomb::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	env.scen->window.draw(sprite);
}
