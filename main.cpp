#include "Environment.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Player.h"
#include "Animation.h"
#include "Bomb.h"
int main()
{
	env.scen->addPlayer(PLAYER2, 2, 2);
	env.scen->addPlayer(PLAYER3, 14, 14);
	env.scen->addPlayer(PLAYER1, 2, 14);
	env.scen->addPlayer(PLAYER4, 14, 2);

	Player* player = env.scen->getGamer();
	env.scen->loadMap();

	while (env.scen->window.isOpen())
	{
		env.checkTick();
		sf::Event event;
		while (env.scen->window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) env.scen->window.close();
			else if (event.type == sf::Event::KeyPressed) eventHandler.onButtonPressed(event.key.code);
			else if (event.type == sf::Event::KeyReleased) eventHandler.onButtonReleased(event.key.code);
		}
	}
	return 0;
}