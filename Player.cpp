#include "Environment.h"
#include "Player.h"
#include "scene.h"
#include "Bomb.h"
#include "Item.h"
#include <iostream>
#include "Bomb.h"
#include <iostream>
#include "Random.h"
void operator++ (PlayerNumber& num)
{ 
	uint16_t i = num;
	++i;
	if (i >= PLAYER_LAST)
		i = PLAYER1;
	num = (PlayerNumber)i;
}

bool _slotInicialized = false;
Player::Player(PlayerNumber pNumber)
{
	if (!_slotInicialized)
	{
		_slotInicialized = true;
		eventHandler.onStepIn.connect(stepIn);
		eventHandler.onSpawn.connect(spawn);
		eventHandler.onDie.connect(die);
	}
	texture = env.getTexture(pNumber);
	sprite.setTexture(*texture);
	sprite.setTextureRect(sf::IntRect(0, 32, P_WIDH, P_HIGH));

	status = STOP;
	frame = 0;
	speed = 2;
	actualDir = DOWN;
	pos.x = pos.y = 64;
	sprite.setPosition(pos.x, pos.y);
	animTimer = sf::seconds(0);
	canSwitch = true;
	hp = 5;
	bombs = 1;
	bombRange = 2;
}

/* SLOTS */
#define HASTE 5
#define EXTRA_BOMB 4
#define EXTRA_RANGE 6
void Player::stepIn(Player * who, sf::Vector2<sf::Uint16> toPos, Dir dir)
{
	Item* itm = nullptr;
	if (itm = env.scen->isItemWithId(toPos, HASTE))
	{
		if(who->speed < 8)
			who->speed += 1;
		env.scen->delItemWithUid(toPos, itm->uid);
	}
	else if (itm = env.scen->isItemWithId(toPos, EXTRA_BOMB))
	{
		who->addBombs(1);
		env.scen->delItemWithUid(toPos, itm->uid);
	}
	else if (itm = env.scen->isItemWithId(toPos, EXTRA_RANGE))
	{
		who->bombRange += 1;
		env.scen->delItemWithUid(toPos, itm->uid);
	}
	else if (itm = env.scen->isItemWithId(toPos, BOMB_ID))
	{
		if (itm->getBomb()->isHurtin()) --(who->getHp());
	}
	if (env.scen->isDangAnimOnScene(toPos))
	{
		if (who->dmgInterval.getElapsedTime().asMilliseconds() > DMG_DEAL_INTERVAL)
		{
			--(who->getHp());
			who->dmgInterval.restart();
		}
	}

	if (who->status == STOP && who !=env.scen->getGamer())
		who->Think();

}

void Player::spawn(Player * player, sf::Vector2<sf::Uint16> toPos)
{
	if (player != env.scen->getGamer() && (env.scen->getPlayersOnScene() == 2))
		eventHandler.onThink.connect(think);
}

void Player::die(const Player * player)
{
	if ((env.scen->getPlayersOnScene() - 1 <= 1) && (player != env.scen->getGamer()))
		eventHandler.onThink.disconnect(think);
}

void Player::think(void)
{
	for (std::list<Player*>::iterator it = env.scen->pBegin(); it != env.scen->pEnd(); ++it)
	{
		Player* pl = *it;
		if ((pl != env.scen->getGamer()) && (pl->status == STOP))
			pl->Think();
	}
}
/* SLOTS */

void Player::Think()
{
	auto x = takeNearest();
	if (inDang()) 
	{ 
		run(x.first);
	}
	else
	{
		if (x.second != A_NONE)
		{
			goToPos(x.first,x.second);
		}
	}
	if (!willBeInDang() && onBombRange(x.first) && (x.second == A_BARREL || x.second == A_PLAYER))
	{
		placeBomb();
	}
}

bool Player::onBombRange(sf::Vector2<sf::Uint16> pos) const
{
	Bomb* b = new Bomb;
	b->setRange(bombRange);
	b->setIMapPos(getPosOnMap());
	b->update();
	if (b->simExpl(pos))
	{
		return true;
	}
	delete b;
	return false;
}

void Player::goToPos(sf::Vector2<sf::Uint16> pos, uint8_t type)
{
	auto x = env.ai.A_getTrace(this->getPosOnMap(), pos);
	if (!x.size())
		x = env.ai.A_getSafeNearest(this->getPosOnMap()).first;
	sf::Vector2<sf::Uint16> step;
	if (x.size())
		step = x.front().vec();
	else
		step = pos;
	sf::Vector2<sf::Int32> to = sf::Vector2<sf::Int32>(step.x, step.y);
	sf::Vector2<sf::Int32> im = sf::Vector2<sf::Int32>(getPosOnMap().x, getPosOnMap().y);
	if (!env.scen->isDangAnimOnScene(step) && !env.scen->willBeDangAnimOnSceneInNextTick(step))
	{
		Dir stepDir = getStepDir(im, to);
		startMoving(stepDir);
	}	
}

std::pair<sf::Vector2<sf::Uint16>,uint8_t> Player::takeNearest()
{
	auto temp = env.ai.A_getNearest(getPosOnMap());
	auto que = temp.first;
	auto type = temp.second;
	if (que.size())
	{
		auto vec = que.back().vec();
		return std::pair<sf::Vector2<sf::Uint16>, uint8_t>(vec, (uint8_t)type);
	}
	return std::pair<sf::Vector2<sf::Uint16>, uint8_t>(sf::Vector2<sf::Uint16>(0,0), A_NONE);
}

bool Player::isNextToDest(sf::Vector2<sf::Uint16> x)
{
	sf::Vector2<sf::Int16> dest(x.x, x.y);
	sf::Vector2<sf::Int16> im(getPosOnMap().x, getPosOnMap().y);
	sf::Vector2<sf::Int16> dif = dest - im;
	if(abs(dif.x) + abs(dif.y) == 1)
		return true;
	return false;
}

bool Player::haveSafeTrace(sf::Vector2<sf::Uint16> x)
{
	auto temp = env.ai.A_getSafeTrace(getPosOnMap(),x,2);
	if (temp.size())
	{
		return true;
	}
	return false;
}

bool Player::willBeInDang()
{
	Bomb* b = new Bomb;
	b->setRange(bombRange);
	b->setIMapPos(getPosOnMap());
	b->update();
	auto temp = env.ai.A_getFutherEscape(getPosOnMap(),b);
	delete b;
	if (temp.size())
	{
		return false;
	}
	return true;
}

void Player::run(sf::Vector2<sf::Uint16> pos)
{
	AI* ai = &env.ai;
	auto que = ai->A_getEscape(getPosOnMap());
	if (!que.size()) return;
	_MapPoint step = que.front();

	sf::Vector2<sf::Int32> to = sf::Vector2<sf::Int32>(step.x, step.y);
	sf::Vector2<sf::Int32> im = sf::Vector2<sf::Int32>(getPosOnMap().x, getPosOnMap().y);

	Dir stepDir = getStepDir(im, to);
	startMoving(stepDir);
}

Dir Player::getStepDir(sf::Vector2<sf::Int32> from, sf::Vector2<sf::Int32> to)
{
	sf::Vector2<int32_t> dif = sf::Vector2<int32_t>(to.x - from.x, to.y - from.y);
	sf::Vector2<int32_t> versor;
	if (dif.x == 0) versor.x = 0;
	else if (dif.x > 0) versor.x = 1;
	else if (dif.x < 0) versor.x = -1;
	if (dif.y == 0) versor.y = 0;
	else if (dif.y > 0) versor.y = 1;
	else if (dif.y < 0) versor.y = -1;

	return versorToDir(sf::Vector2<sf::Int16>(versor.x, versor.y));
}


bool Player::inDang()
{
	std::list<Bomb*> bombs = env.scen->getBombs();
	for (std::list<Bomb*>::iterator it = bombs.begin(); it != bombs.end();++it)
	{
		Bomb* b = *it;
		if(!b->isSafe())
			if (b->simExpl(getPosOnMap()))
				return true;
	}
	if (env.scen->isDangAnimOnScene(getPosOnMap())) return true;
	if (env.scen->willBeDangAnimOnSceneInNextTick(getPosOnMap())) return true;
	return false;
}

Player::~Player()
{
}

void Player::draw() const
{
	env.scen->window.draw(sprite);
}

void Player::startMoving(Dir dir)
{
	if (!willCollide(dir))
	{
		if (status == STOP)
		{
			animTimer = animTimer.Zero;
			status = GOING;
			actualDir = dir;
			frame = 1;
			moveStatic(dir);
		}
		else if (status != STOP)
		{
			if (actualDir != dir)
				status = WANT_CHANGE;
			toChangeDir = dir;
		}
	}
	else
	{
		if (status == STOP)
			actualDir = dir;
	}
}

void Player::moveStatic(Dir dir,sf::Vector2f* position)
{
	if (!position) position = &pos;
	switch (dir)
	{
	case UP:
		position->y -= FRAME;
		break;
	case DOWN:
		position->y += FRAME;
		break;
	case LEFT:
		position->x -= FRAME;
		break;
	case RIGHT:
		position->x += FRAME;
		break;
	}
}

void Player::update()
{
	if (status == STOP)
	{
		frame = 0;
		animTimer = animTimer.Zero;
	}
	else
	{
		if ((frame >0) && (frame < FRAMES_PER_STEP) )
		{
			if (moreThanHalfStep() && canSwitch)
			{
				++frame;
				canSwitch = false;
			}
		}
		if(frame == FRAMES_PER_STEP)
		{
			sf::Vector2f norm = sf::Vector2f(pos.x - getRealPos().x, pos.y - getRealPos().y);
			float param;
			if ((actualDir == LEFT) || (actualDir == RIGHT))
				param = abs(norm.x);
			else
				param = abs(norm.y);

			if (param <= 3)
			{
				frame = 1;
				sprite.setPosition(pos);
				canSwitch = true;
				if (this != env.scen->getGamer()) status = STOP;
				if (status == GOING)
				{
					if (!willCollide(actualDir)) moveStatic(actualDir);
					else status = STOP;
				}
				else if (status == WANT_CHANGE)
				{
					status = STOP;
					startMoving(toChangeDir);
					status = STOPPING;
				}
				else if (status == STOPPING)
				{
					status = STOP;
					frame = 0;
					sprite.setPosition(pos);
				}
				eventHandler.onStepIn(this, getPosOnMap(), actualDir);
			}
		}
		animTimer = sf::milliseconds(TICK);
		switch (actualDir)
		{
		case UP:
			sprite.move(0,-1*animTimer.asSeconds()*speed*FRAME);
			break;
		case DOWN:
			sprite.move(0, 1 * animTimer.asSeconds()*speed*FRAME);
			break;
		case LEFT:
			sprite.move(-1 * animTimer.asSeconds()*speed*FRAME,0);
			break;
		case RIGHT:
			sprite.move(1 * animTimer.asSeconds()*speed*FRAME, 0);
			break;
		}
	}
	sprite.setTextureRect(sf::IntRect(frame* FRAME, FRAME * actualDir, P_WIDH, P_HIGH));
}

void Player::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	env.scen->window.draw(sprite);
}

void Player::placeBomb()
{
	if (bombs)
	{
		sf::Vector2<sf::Uint16> bPos = getPosOnMap();
		Bomb* newBomb = new Bomb;
		newBomb->setAnimPos(sf::Vector2f((float)bPos.x * FRAME,(float) bPos.y * FRAME));
		newBomb->setOwner(this);
		newBomb->setRange(bombRange);
		env.scen->addItem(newBomb, bPos);
		--bombs;
	}
}

const bool Player::willCollide(Dir dir)
{
	sf::Vector2f newPos = pos;
	moveStatic(dir, &newPos);
	if ((newPos.x <ARENA_TOP_X) || (newPos.y <ARENA_TOP_Y) || (newPos.x > ARENA_BOTTOM_X) || (newPos.y > ARENA_BOTTOM_Y))
		return true;
	if (env.scen->isBlockingItem((uint32_t)newPos.x / FRAME, (uint32_t)newPos.y / FRAME))
		return true;
	if (env.scen->getPlayer(sf::Vector2<sf::Uint16>((uint32_t)newPos.x / FRAME, (uint32_t)newPos.y / FRAME)) != nullptr)
		return true;
	return false;
}

const bool Player::moreThanHalfStep() const
{
	sf::Vector2f norm = sf::Vector2f(pos.x - getRealPos().x, pos.y - getRealPos().y);
	if ((actualDir == LEFT) || (actualDir == RIGHT))
	{
		if (abs(norm.x) <= FRAME / 2) return true;
	}
	else
	{
		if (abs(norm.y) <= FRAME / 2) return true;
	}
	return false;
}

