#pragma once
#include "Item_type.h"
#include "Animation.h"
#include "Player.h"
#include "Item.h"
#define BOMB_ID 3
#define EXPLODING_FRAME 4
#define VERTICAL_TEXTURE 11
#define HORIZONTAL_TEXTURE 10
#define LEFT_TEXTURE 13
#define RIGHT_TEXTURE 12
#define UP_TEXTURE 14
#define DOWN_TEXTURE 15
#define SMOKE_FRAMES 8 

class Bomb :
	public Item, public Animation
{
public:
	Bomb();
	~Bomb();

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual sf::Uint16 update();

	void explode();

	bool isExploded() const { return exploded; }
	bool isHurtin() { return isHurting; }
	bool isSafe() { if (frame >= 8+1)  return true;return false; }
	bool simExpl(sf::Vector2<sf::Uint16> pPos);
	bool willExplodeInNextTick() { if (frame == EXPLODING_FRAME-1 ) return true;return false; }

	void setRange(sf::Uint16 rg) { range = rg; }
	const sf::Uint16 getRange() const { return range; }
	void setOwner(Player* player) { owner = player; }
	Player* const getOwner() const { return owner; }
	virtual Bomb* getBomb() { return this; }
	virtual const Bomb* getBomb() const { return this; }

private:
	sf::Uint16 range;
	bool exploded;
	bool isHurting;
	Player* owner;

	bool animOutOfScreen(const sf::Vector2<sf::Uint16> pos);
	sf::Vector2<sf::Int16> getVersor(Dir dir);
};

