#include "EventHandler.h"
#include "Environment.h"
#include <iostream>

EventHandler eventHandler;
EventHandler::EventHandler()
{
	onStepIn.connect(&EventHandler::stepIn);//
	onItemDestroy.connect(&EventHandler::itemDestroy);//
	onSpawn.connect(&EventHandler::spawn);//
	onThink.connect(&EventHandler::think);//
	onDie.connect(&EventHandler::die);//
	onAnimAdd.connect(&EventHandler::animAdd);//
	onTick.connect(&EventHandler::tick);//
	onButtonPressed.connect(&EventHandler::pressButton);//
	onButtonReleased.connect(&EventHandler::releaseButton);//
}


EventHandler::~EventHandler()
{
}

void EventHandler::tick()
{
	env.scen->update();
	env.scen->window.clear();
	env.scen->print();

	env.scen->window.display();
	env.tickTimer.restart();
}

void EventHandler::pressButton(sf::Keyboard::Key key)
{
	Player* player = env.scen->getGamer();
	switch (key)
	{
	case sf::Keyboard::W:player->startMoving(UP);
		break;
	case sf::Keyboard::S:player->startMoving(DOWN);
		break;
	case sf::Keyboard::A:player->startMoving(LEFT);
		break;
	case sf::Keyboard::D:player->startMoving(RIGHT);
		break;
	case sf::Keyboard::C:player->placeBomb();
		break;
	}
}

void EventHandler::releaseButton(sf::Keyboard::Key key)
{
	Player* player = env.scen->getGamer();
		switch (key)
		{
		case sf::Keyboard::W:
			if (player->getActualDir() == UP) player->stop();
			break;
		case sf::Keyboard::S:
			if (player->getActualDir() == DOWN) player->stop();
			break;
		case sf::Keyboard::A:
			if (player->getActualDir() == LEFT) player->stop();
			break;
		case sf::Keyboard::D:
			if (player->getActualDir() == RIGHT) player->stop();
			break;
		}
}

void EventHandler::stepIn(Player* who, sf::Vector2<sf::Uint16> toPos, Dir dir)
{
}

void EventHandler::itemDestroy(Item* itm, sf::Vector2<sf::Uint16> Pos)
{
}

void EventHandler::spawn(Player * player, sf::Vector2<sf::Uint16> toPos)
{
}

void EventHandler::think(void)
{
}

void EventHandler::die(const Player * player)
{
}

void EventHandler::animAdd(Animation * anim)
{
	if (anim->isHurting())
	{
		if (Player* player = env.scen->getPlayer(anim->getMapPos()))
			--(player->getHp());
	}
}
