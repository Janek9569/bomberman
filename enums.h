#pragma once
#include <map>
#include <string>
#include <SFML\Graphics.hpp>

enum Prop
{
	DESTROYABLE,
	IS_BONUS,
	CAN_STEP_IN,
	CAN_THROW_BOMB,
	BLOCKING_STACK,
	PROP_LAST
};

extern std::map<std::string, Prop> str2prop;
enum Dir
{
	DOWN,
	LEFT,
	RIGHT,
	UP,
	DIR_LAST
};
enum Orientation
{
	HORIZONTAL,
	VERTICAL,
	ORIENTATION_LAST
};
Orientation getOrientation(Dir dir);

enum Status {
	GOING,
	STOP,
	STOPPING,
	WANT_CHANGE
};

Dir versorToDir(sf::Vector2<sf::Int16> versor);