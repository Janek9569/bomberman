#include "Environment.h"
#include "Items.h"
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <fstream>
#include <iostream>
#include "enums.h"


Items::Items()
{
	str2prop["DESTROYABLE"] = DESTROYABLE;
	str2prop["IS_BONUS"] = IS_BONUS;
	str2prop["CAN_STEP_IN"] = CAN_STEP_IN;
	str2prop["CAN_THROW_BOMB"] = CAN_THROW_BOMB;
	str2prop["PROP_LAST"] = PROP_LAST;
	str2prop["BLOCKING_STACK"] = BLOCKING_STACK;
}


Items::~Items()
{
}

void Items::loadItemsType()
{
	std::string fDir = "Items/";
	boost::filesystem::path p(fDir);
	boost::filesystem::directory_iterator end_iter;
	if (exists(p) && is_directory(p))//folder istnieje
		for (boost::filesystem::directory_iterator it = boost::filesystem::directory_iterator(p); it != end_iter;it++)
			if (boost::filesystem::is_regular_file(it->status()))
			{
				std::string name = it->path().filename().string();
				Item_type newItem;
				volatile std::string name2 = fDir + name;
				std::ifstream file(fDir + name);
				std::string str;
				if (file >> str)
				{
					if (str != "ID")
					{
						std::cerr << "ID problem in Items::loadItemsType()\n";
						continue;
					}
					else
					{
						if(file >> str)
							newItem._setId(atoi(str.c_str()));
						else
						{
							std::cerr << "ID problem in Items::loadItemsType()\n";
							continue;
						}
					}
					if (file >> str)
					{
						if (str != "TEXTURE")
						{
							std::cerr << "TEXTURE problem in Items::loadItemsType()\n";
							continue;
						}
						else
						{
							if (file >> str)
								newItem._setTexture(env.getTexture(atoi(str.c_str())));
							else
							{
								std::cerr << "TEXTURE problem in Items::loadItemsType()\n";
								continue;
							}
						}
					}
				}
				std::map<std::string, Prop>::iterator it;
				while (file >> str)
				{
					if ((it = str2prop.find(str)) != str2prop.end())
					{
						file >> str;
						bool prope = false;
						if (str == "true" || str == "TRUE") prope = true;
						else if (str == "false" || str == "FALSE") prope = false;
						else
						{
							std::cout << "cant recognaise symbol " << str << std::endl;
							continue;
						}
						newItem._getProp(it->second) = prope;
					}
					else
						std::cout << "cant recognaise symbol " << str << std::endl;
				}
				file.close();
				items[newItem.getId()] = newItem;
			}
}
