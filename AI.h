#pragma once
#include <SFML\Graphics.hpp>
#include <list>
#include <memory>
typedef struct A_field
{
	A_field(uint16_t _x, uint16_t _y, sf::Vector2<sf::Uint16> _dest) { x = _x; y = _y; parent = nullptr;cost = 1; het = _getHet(_dest); dest = _dest; }
	~A_field() { }
	uint16_t x;
	uint16_t y;
	uint16_t het;
	uint16_t cost;
	sf::Vector2<sf::Uint16> dest;
	sf::Vector2<sf::Uint16> getPosOnMap() const { return sf::Vector2<sf::Uint16>(x, y); }
	uint16_t _getHet(sf::Vector2<sf::Uint16> dest) { return (abs(dest.x - x) + abs(dest.y - y)); }
	uint16_t _getCost() { return 1; }
	uint16_t getTotalCost() { return (het + cost); }
	void update() { het = _getHet(dest); cost = _getCost(); }
	sf::Vector2<sf::Uint16> vec() const {return sf::Vector2<sf::Uint16>(x, y);}
	std::shared_ptr<A_field> parent;

	uint16_t _howmManyFields();
	bool A_canStep() const;
	bool A_isBrick() const;
	bool A_inDang(Bomb* bb = nullptr);
	bool A_isEmpty() const;
	bool A_isPlayer() const;
	bool A_isMined() const;
}A_field;

typedef struct _MapPoint
{
	_MapPoint(A_field* f) { x = f->x; y = f->y; }
	sf::Uint16 x;
	sf::Uint16 y;
	operator sf::Vector2<sf::Uint16> const()
	{
		return sf::Vector2<sf::Uint16>(x, y);
	}
	sf::Vector2<sf::Uint16> vec() { return sf::Vector2<sf::Uint16>(x, y); }
}_MapPoint;

enum FollowType
{
	A_NONE,
	A_PLAYER,
	A_BONUS,
	A_BARREL,
	A_FOLLOW_END
};

class AI
{
public:
	AI();
	~AI();

	bool A_isTrace(sf::Vector2<sf::Uint16> frm, sf::Vector2<sf::Uint16> to) { if (A_getTrace(frm, to).size() == 0) return false;return true; }
	std::list<_MapPoint> A_getTrace(sf::Vector2<sf::Uint16> frm, sf::Vector2<sf::Uint16> to);
	std::list<_MapPoint> A_getEscape(sf::Vector2<sf::Uint16> frm);
	std::list<_MapPoint> A_getFutherEscape(sf::Vector2<sf::Uint16> frm, Bomb* b);
	std::pair<std::list<_MapPoint>, FollowType> A_getNearest(sf::Vector2<sf::Uint16> frm);
	std::pair<std::list<_MapPoint>, FollowType> A_getSafeNearest(sf::Vector2<sf::Uint16> frm);
	std::list<_MapPoint> A_getSafeTrace(sf::Vector2<sf::Uint16> frm, sf::Vector2<sf::Uint16> to, uint8_t dontCare=0);
private:
	std::shared_ptr<A_field> _getLowestTotalScore(std::list<std::shared_ptr<A_field>>& A_openList);
	std::shared_ptr<A_field> _getLowestEscapeScore(std::list<std::shared_ptr<A_field>>& A_openList);
	bool _isOnList(std::shared_ptr<A_field> field, std::list<std::shared_ptr<A_field>>& list);
	void _moveToList(std::shared_ptr<A_field> x, std::list<std::shared_ptr<A_field>>& from, std::list<std::shared_ptr<A_field>>& to);
	std::list<_MapPoint> _makeTrace(std::shared_ptr<A_field> dest);
	std::list<std::shared_ptr<A_field>> _getNeighbors(std::shared_ptr<A_field> field);
	FollowType _isThig(std::shared_ptr<A_field> p);
};

bool operator==(const std::shared_ptr<A_field> x, const std::shared_ptr<A_field> y);

bool operator==(const _MapPoint x, const _MapPoint y);


