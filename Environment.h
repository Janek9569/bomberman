#pragma once
#include "EventHandler.h"
#include <map>
#include <memory>
#include "SFML\Graphics.hpp"
#include "Items.h"
#include "scene.h"
#include "AI.h"
#define THINK_INTERVAL 500
#define TICK 15
extern sf::Texture* invisible;
class Environment : public sf::Clock
{
public:
	Environment();
	~Environment();

	Item_type* const  getItemType(sf::Uint32 id) { return &(items.items[id]); }
	scene* scen;
	AI ai;
	sf::Texture* const getTexture(uint32_t id)  { return &Textures[id]; }
	void loadTextures();

	bool isThinkin() { return (thinkTimer.getElapsedTime().asMilliseconds() >= THINK_INTERVAL); }
	sf::Clock thinkTimer;

	sf::Clock tickTimer;
	void checkTick() { if (tickTimer.getElapsedTime().asMilliseconds() >= TICK) eventHandler.tick(); }
private:
	std::map<uint32_t, sf::Texture> Textures;
	Items items;
};

extern Environment env;
