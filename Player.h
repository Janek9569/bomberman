#pragma once
#include "enums.h"
#include <list>
#include <SFML\Graphics.hpp>
#define PLAYER_MOVING_TEXTURE 0
#define P_WIDH 32
#define P_HIGH 32
#define FRAME 32
#define FRAMES_PER_STEP 2
#define DMG_DEAL_INTERVAL 1000
class Item;
class scene;

enum PlayerNumber
{
	PLAYER1 = 0,
	PLAYER2,
	PLAYER3,
	PLAYER4,
	PLAYER_LAST
};
void operator++ (PlayerNumber& num);

class Player: public sf::Drawable
{
public:
	Player(PlayerNumber pNumber);
	~Player();
	void draw() const ;
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	const Dir getActualDir() const { return actualDir; }
	void setSpeed(float newSpeed) { speed = newSpeed; }
	void startMoving(Dir dir);
	void stop() { if (status == GOING) status = STOPPING; }
	void setStatic() { sprite.setPosition(pos); sprite.setTextureRect(sf::IntRect(0, 0, P_WIDH, P_HIGH)); }
	void update();

	void setPosOnMap(sf::Uint16 x, sf::Uint16 y) { status = STOP; pos.x = (sf::Uint16)((float)x*FRAME); pos.y = (sf::Uint16)((float)y*FRAME); sprite.setPosition(pos.x, pos.y); }
	const sf::Vector2<sf::Uint16> getPosOnMap() const { return sf::Vector2<sf::Uint16>((sf::Uint16)(pos.x/FRAME),(sf::Uint16)(pos.y/FRAME)); }
	const sf::Vector2f getRealPos() const { return sprite.getPosition(); }
	const sf::Vector2f getPos() const { return pos; }

	const sf::Uint8 getHp() const { return hp; }
	sf::Uint8& getHp() { return hp; }
	void setHp(sf::Uint8 newHp) { hp = newHp; }

	sf::Clock dmgInterval;

	const sf::Uint8 getBombsAmount() const { return bombs; }
	void addBombs(sf::Uint8 amount) { bombs += amount; }
	
	void placeBomb();

	bool inDang();
protected:
	int frame;
private:
	void Think();

	sf::Uint8 hp;
	sf::Uint8 bombs;
	sf::Vector2f pos;
	float speed;
	sf::Uint16 bombRange;

	Status status;
	Dir actualDir;
	Dir toChangeDir;
	bool canSwitch;

	sf::Sprite sprite;
	sf::Texture* texture;
	sf::Time animTimer;


	const bool willCollide(Dir dir);
	bool willBeInDang();
	const bool moreThanHalfStep() const;
	Dir getStepDir(sf::Vector2<sf::Int32> from, sf::Vector2<sf::Int32> to);

	void moveStatic(Dir dir, sf::Vector2f* position = nullptr);
	void goToPos(sf::Vector2<sf::Uint16> pos, uint8_t type);
	std::pair<sf::Vector2<sf::Uint16>, uint8_t> takeNearest();
	bool isNextToDest(sf::Vector2<sf::Uint16> x);
	bool haveSafeTrace(sf::Vector2<sf::Uint16> x);
	bool onBombRange(sf::Vector2<sf::Uint16> pos) const;
	void run(sf::Vector2<sf::Uint16> pos);


	/* SLOTS */
	static void stepIn(Player* who, sf::Vector2<sf::Uint16> toPos, Dir dir);
	static void spawn(Player* player, sf::Vector2<sf::Uint16> toPos);
	static void think();
	static void die(const Player* player);
	/* SLOTS */
};

